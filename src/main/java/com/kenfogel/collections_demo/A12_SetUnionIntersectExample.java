package com.kenfogel.collections_demo;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Shows the intersection (common elements) and union (all elements but no
 * duplicates) of two sets
 *
 * @author Ken Fogel
 */
public class A12_SetUnionIntersectExample {

    public void perform() {

        System.out.println("\n>>>> A12_SetUnionIntersectExample\n");
        
        // Create two sets.
        Set<String> s1 = new HashSet<>();
        s1.add("Sherlock Holmes");
        s1.add("Doctor Watson");
        s1.add("Inspector Lestrad");

        Set<String> s2 = new HashSet<>();
        s2.add("Sherlock Holmes");
        s2.add("Mycroft Holmes");

        Set<String> union = new TreeSet<>(s1);
        union.addAll(s2);    // now contains the union

        print("union", union);

        Set<String> intersect = new TreeSet<>(s1);
        intersect.retainAll(s2);

        print("intersection", intersect);

    }

    /**
     * Display the set
     * @param label
     * @param data 
     */
    private void print(String label, Collection<String> data) {

        System.out.println("--------------" + label + "--------------");

        Iterator<String> it = data.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
