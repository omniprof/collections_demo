package com.kenfogel.collections_demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * This class demonstrates the basic Collections interface that all sequential
 * data structures implement. This interface supports adding objects to the
 * collection and searching for objects to verify their existence or to remove
 * objects from the collection. What it does not support is random access to
 * elements.
 *
 * To be able to traverse* a Collection you have two approaches. This example
 * uses the toArray() method to convert the Collection into a simple array.
 *
 * *traverse: access every element in a collection from the first element to
 * the last element.
 *
 * https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html
 *
 * @author Ken Fogel
 */
public class A01_CollectionExample {

    public void perform() {
        
        System.out.println("\n>>>> A01_CollectionExample\n");
        String names[] = {"Mercury", "Venus", "Earth", "Mars", "Jupiter",
            "Saturn", "Uranus", "Neptune", "Pluto"};

        Collection<String> planets = new ArrayList<>();

        for (int i = 0, n = names.length; i < n; i++) {
            planets.add(names[i]);
        }

//        Collection<String> planets = new ArrayList<>(Arrays.asList(names));


        String s[] = planets.toArray(new String[0]);

        for (int i = 0, n = s.length; i < n; i++) {
            System.out.println(s[i]);
        }

        planets.remove(names[3]);

        System.out.println(names[1] + " " + planets.contains(names[1]));
        System.out.println(names[3] + " " + planets.contains(names[3]));

    }
}
