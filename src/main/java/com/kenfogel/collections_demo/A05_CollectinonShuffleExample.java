package com.kenfogel.collections_demo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The Collections shuffle algorithm randomly rearranges the objects in the
 * collection.
 *
 * @author Ken Fogel
 */
public class A05_CollectinonShuffleExample {

    public void perform() {
        
        System.out.println("\n>>>> A05_CollectinonShuffleExample\n");
       
        String[] strArray = new String[]{"Java", "Perl", "Python", "Ruby",
            "PHP", "Rails"};

        List<String> l = Arrays.asList(strArray);
        System.out.println("Before shuffle: " + l);
        Collections.shuffle(l);
        System.out.println(" After shuffle: " + l);
    }
}
