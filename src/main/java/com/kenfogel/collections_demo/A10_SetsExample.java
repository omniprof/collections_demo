package com.kenfogel.collections_demo;

/**
 * A Set is an interface for implementations that require every element in the
 * collection to be unique
 * 
 * https://docs.oracle.com/javase/8/docs/api/java/util/Set.html
 * https://docs.oracle.com/javase/8/docs/api/java/util/HashSet.html
 * https://docs.oracle.com/javase/8/docs/api/java/util/TreeSet.html
 * 
 *
 * @author Ken Fogel
 */
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class A10_SetsExample {

    public void perform() {

        System.out.println("\n>>>> A10_SetsExample\n");

        // Most efficient implentation is the HashSet
        Set<String> hs = new HashSet<>();

        hs.add("one");
        hs.add("two");
        hs.add("three");

        System.out.println("Here is the HashSet: " + hs);

        if (!hs.add("three")) {
            System.out.println("Attempt to add duplicate. " + "Set is unchanged: " + hs);
        }

        // The TreeSet uses the object rather than the hashcode to determine if 
        // the object already exists in the set.
        Set<String> ts = new TreeSet<>();

        ts.add("one");
        ts.add("two");
        ts.add("three");

        System.out.println("Here is the TreeSet: " + ts);

        if (!ts.add("three")) {
            System.out.println("Attempt to add duplicate. " + "Set is unchanged: " + ts);
        }

    }
}
