package com.kenfogel.collections_demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Here we have more examples of the algorithms available from the Collections
 * class. These examples use the List interface that adds to a collection the
 * ability to perform random access operations
 *
 * https://docs.oracle.com/javase/8/docs/api/java/util/List.html
 *
 * @author Ken Fogel
 */
public class A04_CollectionUtilitiesExample {

    public void perform() {
        
        System.out.println("\n>>>> A04_CollectionUtilitiesExample\n");
        
        // Use Arrays algorithm to convert an array into an object that supports the List interface
        List<String> list1 = Arrays.asList("one Four five six Two three Four five six one".split(" "));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("one Four five six Two three Four five six one".split(" ")));
        System.out.println("List1: " + list1);
        System.out.println("List2: " + list2);
        System.out.println("List1 max: " + Collections.max(list1));
        System.out.println("List1 min: " + Collections.min(list1));
        System.out.println("List2 max: " + Collections.max(list2));
        System.out.println("List2 min: " + Collections.min(list2));

        // Certain algorithms such as min and max require a comparator
        AlphabeticComparator comp = new AlphabeticComparator();
        System.out.println("List1 max w/ comparator: " + Collections.max(list1, comp));
        System.out.println("List1 min w/ comparator: " + Collections.min(list1, comp));
        System.out.println("List2 max w/ comparator: " + Collections.max(list2, comp));
        System.out.println("List2 min w/ comparator: " + Collections.min(list2, comp));

        // Searching for one list inside another
        List<String> sublist = Arrays.asList("Four five six".split(" "));
        System.out.println("List1 indexOfSubList: "
                + Collections.indexOfSubList(list1, sublist));
        System.out.println("List1 lastIndexOfSubList: "
                + Collections.lastIndexOfSubList(list1, sublist));

        // Replace algorithm thats searches for the object in the Collection and then replaces it
        Collections.replaceAll(list1, "one", "Yo");
        System.out.println("List1 replaceAll: " + list1);

        // Reverse algorithm
        Collections.reverse(list2);
        System.out.println("List2 reverse: " + list2);

        // Rotate algorithm, moves elements to the right and those that exceed
        // the length of the Collection move to the front. Creates a circular
        // structure
        Collections.rotate(list2, 3);
        System.out.println("List2 rotate: " + list2);

        // Copy algorithm
        List<String> source = Arrays.asList("in the matrix".split(" "));
        Collections.copy(list1, source);
        System.out.println("List1 copy: " + list1);

        // Swap algorithm swaps elements in the collection
        Collections.swap(list1, 0, list1.size() - 1);
        System.out.println("List1 swap: " + list1);

        // Fill algorithm fills a collection with a specified object
        Collections.fill(list1, "pop");
        System.out.println("List1 fill: " + list1);

        // nCopies algorithm returns a collection with a specified object
        // repeated in the collection
        List<String> dups = Collections.nCopies(3, "snap");
        System.out.println("dups: " + dups);
    }
}

/**
 * A Comparator is an object that can compare objects. As it exists outside of
 * the objects you can have multiple comparators or rules for different
 * purposes. The alternative is to implement the Comparable interface in the
 * objects but this means there can only be a single rule for comparisons.
 *
 * @author Ken Fogel
 */
class AlphabeticComparator implements Comparator<Object> {

    public int compare(Object o1, Object o2) {
        String s1 = (String) o1;
        String s2 = (String) o2;
        return s1.toLowerCase().compareTo(s2.toLowerCase());
    }
}
