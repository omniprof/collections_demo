package com.kenfogel.collections_demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Like the Arrays class of algorithms, there is a Collections class with
 * algorithms that perform actions on any class that supports the Collection
 * interface
 *
 * https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html
 *
 * @author Ken Fogel
 */
public class A03_ApplyCollectionAlgorithmsToArray {

    public void perform() {
        
        System.out.println("\n>>>> A03_ApplyCollectionAlgorithmsToArray\n");

        String n[] = new String[]{"John", "Lennon", "Karl", "Marx",
            "Groucho", "Marx", "Oscar", "Grouch"};

        // Arrays algorithm to implement a List interface from a simple array
        List<String> l = Arrays.asList(n);
        System.out.println("List from Array: " + l);
        try {
             l.add("Ken"); // fails because array is fixed length
        } catch (UnsupportedOperationException ex) {
            System.out.println("Adding to a list created from an array fails\n"
                    + "because the array is a fixed length.\n");
        }
        // Collections algorithm to sort a collection
        Collections.sort(l);
        System.out.println(l);
        
        // Arrays algorithm to implement a List interface from a simple array
        List<String> l2 = new ArrayList<>(Arrays.asList(n));
        System.out.println("ArrayList: " + l);

        l2.add("Ken"); 
        System.out.println("Adding to a list created from an ArrayList succeeds.\n");

        System.out.println(l2);
        // Collections algorithm to sort a collection
        Collections.sort(l2);
        System.out.println(l2);
    }
}
