package com.kenfogel.collections_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author omni_
 */
public class CollectionRunner {
    
    private final static Logger LOG = LoggerFactory.getLogger(CollectionRunner.class);

    public void perform() {
        A01_CollectionExample ace01 = new A01_CollectionExample();
        ace01.perform();

//        A02_CollectionVariationExample ace02 = new A02_CollectionVariationExample();
//        ace02.perform();
//
//        A03_ApplyCollectionAlgorithmsToArray ace03 = new A03_ApplyCollectionAlgorithmsToArray();
//        ace03.perform();
//
//        A04_CollectionUtilitiesExample ace04 = new A04_CollectionUtilitiesExample();
//        ace04.perform();
//
//        A05_CollectinonShuffleExample ace05 = new A05_CollectinonShuffleExample();
//        ace05.perform();
//
//        A06_CapacityExample ace06 = new A06_CapacityExample();
//        try {
//            ace06.perform();
//        } catch (NoSuchFieldException | IllegalArgumentException | SecurityException | IllegalAccessException ex) {
//            LOG.error("Error in ace06", ex);
//        }
//
//        A07_StackExample ace07 = new A07_StackExample();
//        ace07.perform();
//
//        A08_QueueExample ace08 = new A08_QueueExample();
//        ace08.perform();
//
//        A09_DequeExample ace09 = new A09_DequeExample();
//        ace09.perform();
//
//        A10_SetsExample ace10 = new A10_SetsExample();
//        ace10.perform();
//
//        A11_SetWithComparatorExample ace11 = new A11_SetWithComparatorExample();
//        ace11.perform();
//
//        A12_SetUnionIntersectExample ace12 = new A12_SetUnionIntersectExample();
//        ace12.perform();
//
//        A13_TreeMapExample ace13 = new A13_TreeMapExample();
//        ace13.perform();
//        
//        A14_HashMapExample ace14 = new A14_HashMapExample();
//        ace14.perform();
    }

    public static void main(String[] args) {
        CollectionRunner cr = new CollectionRunner();
        cr.perform();
        System.exit(0);
    }

}
