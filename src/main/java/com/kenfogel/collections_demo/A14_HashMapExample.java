package com.kenfogel.collections_demo;

import java.util.HashMap;
import java.util.Map;

/**
 * When the order is not important but searching for an object is important use
 * a HashMap
 *
 * https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html
 *
 * @author Ken Fogel
 */
public class A14_HashMapExample {

    public void perform() {

        System.out.println("\n>>>> A14_HashMapExample\n");

        Map<String, String> map = new HashMap<>();

        map.put("Alpha", "Terra");
        map.put("Beta", "Sherlock");
        map.put("Gamma", "Guinness");

        // Old fashioned (up to Java 1.7) loop
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.printf("Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
        }

        // New functional stream notation
        map.entrySet().forEach((entry) -> {
            System.out.printf("Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
        });


        // The toString of a map prints inorder
        System.out.println("Map = " + map);

        // The HashCode values used by the HashMap
        System.out.println("Hash of Alpha: " + "Alpha".hashCode());
        System.out.println(" Hash of Beta: " + "Beta".hashCode());
        System.out.println("Hash of Gamma: " + "Gamma".hashCode());

        // Retrieve a value using the key
        String name = map.get("Beta");
        System.out.println("Value Beta = " + name);
    }
}
